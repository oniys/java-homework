package com.company;

public class Family {
    public static Human[] DataBaseFamily = new Human[1000];
    public static Human[] DataBaseChild = new Human[1000];

    private static int Base_position_family = 0;
    private static int Base_position_child = 0;

     static Human mather;
     static Human father;
     static Human[] children;
     static Pet pet;

    public void ADD_BASE_FAMILY(Human base){
        DataBaseFamily[Base_position_family] = base;
        Base_position_family++;
    }
    public void ADD_BASE_CHILD(Human base){
        DataBaseChild[Base_position_child] = base;
        Base_position_child++;
    }


 Human FamilyAddQuick(String names, String surname, int years, String mothers, String fathers) {

    if (mothers != null && !mothers.trim().isEmpty() || fathers != null && !fathers.trim().isEmpty()) {

        mather = new Human(
                mothers.split(" ")[0].trim(),
                mothers.split(" ")[1].trim(),
                0);
        ADD_BASE_FAMILY(mather);

        father = new Human(
                fathers.split(" ")[0].trim(),
                fathers.split(" ")[1].trim(),
                0);
        ADD_BASE_FAMILY(father);

        Human human1 = new Human(
                names,
                surname,
                years,
                pet,
                mather,
                father
        );
        ADD_BASE_FAMILY(human1);
 return human1;
    }
     return null;
 }


    Human FamilyAddFull(String names, String surname, int years,Byte iqs, Pet pet, String mothers, String fathers,String[][] schedule) {

        if (mothers != null && !mothers.trim().isEmpty() || fathers != null && !fathers.trim().isEmpty()) {

            mather = new Human(
                    mothers.split(" ")[0].trim(),
                    mothers.split(" ")[1].trim(),
                    years);
            ADD_BASE_FAMILY(mather);

            father = new Human(
                    fathers.split(" ")[0].trim(),
                    fathers.split(" ")[1].trim(),
                    years);
            ADD_BASE_FAMILY(father);


            Human human2 = new Human(
                    names,
                    surname,
                    years,
                    iqs,
                    pet,
                    mather,
                    father,
                    schedule
            );
            ADD_BASE_FAMILY(human2);

            return human2;
        }
        return null;
    }


    Human addChild(String names, String surname, int years){
        Human child1 = new Human(   names,
                surname,
                years);
        ADD_BASE_CHILD(child1);
        return child1;
    };


    static String deleteChild(int index){
        DataBaseChild[index] = null;
        for (int i = 2; i < DataBaseChild.length-1; i++) {
            DataBaseChild[i-1] = DataBaseChild[i];
            DataBaseChild[i] = null;
        }

        return "Ребенок удален из базы";
    }
}
