package com.company;

import java.lang.reflect.Array;
import java.util.Arrays;

public class Human {

    private static byte maxIq=100;
    private static byte minIq=0;
    private String name;
    private String surname;
    private String mother;
    private String father;
    private int year;
    private byte iq;
    private Pet pet = new Pet();
    private String schedule;

    public Human(String names, String surnames, int years, byte iqs,Pet pets,  Human mothers, Human fathers, String[][] schedules) {
        name = names;
        surname = surnames;
        year = years;
        iq = (byte) limitIqLevel(iqs);
        pet = pets;
        father = fathers.name + " " + fathers.surname;
        mother = mothers.name + " " + mothers.surname;
        schedule = '\''+", schedule='" + arrysExport(schedules)+ '\'';

    }

     public Human(String names, String surnames, int years) {
        name = names;
        surname = surnames;
        year = years;
        schedule="";
     }

    public Human(String names, String surnames, int years,Pet pets, Human mothers, Human fathers) {
        name = names;
        surname = surnames;
        year = years;
        pet = pets;
        father = fathers.name + " " + fathers.surname;
        mother = mothers.name + " " + mothers.surname;
        schedule="";
    }
    public Human(){
    }

    String getToGreetPet() { return "Привет, "+ pet.getNickname(); }
    String describePet(){ return "У меня есть "+ pet.getSpecies() +
            ", ему " + pet.getAge() + " лет, он " + new Pet().levelTrick(pet.trickLevel); }


    public String arrysExport(String[][] elem) {
      return   Arrays.deepToString(elem);
    };

    @Override
    public String toString() {

        return "Human{"
                + "name='" + name + '\''
                + ", surname='" + surname + '\''
                + ", year=" + year
                + ", iq='" + iq + '\''
                + ", mother='" + mother + '\''
                + ", father='" +  father + '\''
                + ", pet='"  + pet
                +   schedule
                + '}';
    }



    /**
     * по ТЗ - ограничиваем iq  0<->100
     * TODO возможно лучше обеденить limitTrickLevel в класе Pet и limitIqLevel что б не делать дубликат
     * */
    public int limitIqLevel(int value) {
        return (value > maxIq) ? maxIq : (value < minIq ? minIq: value );
    }

    public String getName() {
        return name;
    }

    public void setName(String name) { this.name = name;
    }
    public String getSurname() { return surname; }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public byte getIq() {
        return iq;
    }

    public void setIq(byte iq) {
        this.iq = iq;
    }
    public String getSchedule() {
        return schedule;
    }

    public void setSchedule(String schedule) {
        this.schedule = schedule;
    }


}
