package com.company;

public class Main {

    static Object DATABASE[] = new Object[1000];

    public static void main(String[] args) {

        //TODO обект Pet заполнеными параметрами для теста
        Pet pet1 = new Pet("dog", "Rock", 5, 75, new String[]{"eat", "drink", "sleep"});
        System.out.println("1 : обект Pet заполнеными параметрами для теста: все поля\n" + pet1);

        Pet pet2 = new Pet("cat", "Pushok");
        System.out.println("2 : обект Pet заполнеными параметрами для теста: вид,кличка\n" + pet2);

        Pet pet3 = new Pet();
        System.out.println("3 : обект Pet заполнеными параметрами для теста: пустой\n" + pet3);


        System.out.println(": --Выводим eat :" + pet1.getMessageEat());
        System.out.println(": -- Выводим respond :" + pet2.getMessageRespond());
        System.out.println(": -- Выводим Foul :" + pet3.getMessageFoul());



//        TODO обект Human со всеми заполенными параметрами для теста
        Human obj1 = new Human(
                "Michael",
                "Karleone",
                1977,
                (byte) 90,
                        new Pet(
                                "dog",
                                "Rock",
                                5,
                                75,
                                new String[]{"eat", "drink","sleep"}),
                new Human("Jane","Jane Karleone",1965),
                new Human("Vito","Karleone",1950),
                new String[][]{{"day"},{"task"}});

        System.out.println("\n 4 :Обект Human напрямую  тест: все поля\n" + obj1);




        /** TODO пару примеров для теста
         *  напрмер:.() Быстрое создание семьи
         * */

        Human famQuick = new Family().FamilyAddQuick(
                "Jarbi",
                "Htisto",
                1990,
                "Laura Htisto",
                "Paulo Htisto");

        /**Провивяряем  что при создании семьи семьи:
         * 1. создается  обект Human
         * 2. у него есть родители Mother и Father создаются отдельными обектами
         * 3/1. у созданых Mother и Father = НЕТУ РОДИТЕЛЕЙ - что логично мы их не создавали для них!
         * 3/2. у созданых Mother и Father = НЕТУ ДАТЫ - что логично мы их не создавали для них!
         * 3/3. у созданых Mother и Father = НЕТУ ПИТОМЦА - что логично мы их не создавали для них!
         * 4. не заполненая строка mother и father при создании обекта - не создаст обекты - вернет null
         * 5 Питомца нету и у Human*/

        System.out.println("\n5 : Быстрое создание семьи FamilyAddQuick: \n" + famQuick);
        if (famQuick != null) {
            System.out.println("FATHER: " + Family.father.toString());
            System.out.println("MOTHER: " + Family.mather.toString());
        }


        /** TODO пару примеров для теста
         *  напрмер:.() Полное создание семьи
         * */

        Human famFull= new Family().FamilyAddFull(
                "Jarbi",
                "Enestinis",
                1990,
                (byte) 95,
                new Pet(
                        "Rabbit",
                        "Toto",
                        2,
                        15,
                        new String[]{"eat", "drink","sleep"}),
                "Jana Enestinis",
                "Frans Enestinis",
                new String[][]{{"day","task"},{"day_2","task_2"}});

        System.out.println("\n6 : Полное создание семьи FamilyAddFull: \n" + famFull);
        if (famQuick != null) {
            System.out.println("FATHER: " + Family.father.toString());
            System.out.println("MOTHER: " + Family.mather.toString());
        }


        System.out.println("-- :  Приведствуем питомца :" +  famFull.getToGreetPet());
        System.out.println("-- :  Описать своего любимца :" + famFull.describePet());

        Human addChild= new Family().addChild(
                "child!",
                "Cholds",
                2015
        );
        System.out.println("\n7 :Cоздание ребенка addChild: " + addChild);







        //TODO проверяем все что запмсано базе DataBaseFamily

        System.out.println("\nПроверяю что записано в базе  DataBaseFamily:");
        for (int i = 0; i< Family.DataBaseFamily.length; i++ ){
            if(Family.DataBaseFamily[i] != null){
                System.out.println(Family.DataBaseFamily[i]);
            }
        }

        //TODO проверяем все что записано базе DataBaseChild


        System.out.println("\nПроверяю что записано в базе  DataBaseChild:");
        for (int i = 0; i< Family.DataBaseChild.length; i++ ){
            if(Family.DataBaseChild[i] != null){
                System.out.println(Family.DataBaseChild[i]);
            }
        }

        System.out.println("\n8: Удаление ");
        //удаляем ребенка
        System.out.println(Family.deleteChild(0));
        //проверяем удален ли ребенок
        for (int i = 0; i< Family.DataBaseChild.length; i++ ){
            if(Family.DataBaseChild[i] != null){
                System.out.println(Family.DataBaseChild[i]);
            }
            // можно раскоментировать и пройтись по базе что бы проверить если  там  дети
//            System.out.println("в базе нет детей");
        }


    }
}
