package com.company;
import java.util.Arrays;

public class Pet {

    private static byte maxTrick=100;
    private static byte minTrick=0;

    String species;
    String nickname;
    int age;
    byte trickLevel;
    String[] habits;


    public Pet(String speciess, String nicknames, int ages, int trickLevels, String[] habitss ) {
        this.species = speciess;
        this.nickname = nicknames;
        this.age = ages;
        this.trickLevel = (byte) limitTrickLevel(trickLevels);
        this.habits = habitss;
    }

    public Pet(String speciess, String nicknames) {
        this.species = speciess;
        this.nickname = nicknames;
    }

    public Pet(){}


    String getMessageEat(){ return "Я кушаю!"; }
    String getMessageRespond(){
        return "Привет, хозяин. Я - "+nickname+" . Я соскучился!";
    }
    String getMessageFoul(){ return "Нужно хорошо замести следы..."; }

    @Override
    public String toString() {
        return species+"{"
                + "nickname='" + nickname + '\''
                + ", age=" + age
                + ", trickLevel=" + trickLevel
                + ", habits=" +
                Arrays.toString(habits)
                + '}';
    }

    /**
     * по ТЗ - ограничиваем trickLevel  0<->100
     * TODO возможно лучше обеденить limitIqLevel в класе Humman и limitTrickLevel что б не делать дубликат
     * */
    public int limitTrickLevel(int value) {
        return (value > maxTrick) ? maxTrick : (value < minTrick ? minTrick: value );
    }

    public String levelTrick(int value){
        String trick;
        if(value <= 50){ trick  = "почти не хитрый"; }
        else { trick = "почти не хитрый";
        }
        return trick;
    }

    public String getSpecies() {
        return species;
    }

    public void setSpecies(String species) {
        this.species = species;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public byte getTrickLevel() {
        return trickLevel;
    }

    public void setTrickLevel(byte trickLevel) {
        this.trickLevel = trickLevel;
    }

    public String[] getHabits() {
        return habits;
    }

    public void setHabits(String[] habits) {
        this.habits = habits;
    }

}
