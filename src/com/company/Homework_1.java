package com.company;

import java.util.Scanner;

public class Homework_1 {
    private static Scanner in = new Scanner(System.in);
    private static ArrayMemorableDates resultMemorableDate = new ArrayMemorableDates();
    private static ArrayRunRepetitions resultRunRepetitions = new ArrayRunRepetitions();
    private static String name;
    private static int randomNumberMemory;
    private static int[] userNumberMemory = new int[100];
    private static String[] statArrays = {"L", "E", "T", " ", "T", "H", "E", " ", "G", "A", "M", "E", " ", "B", "E", "G", "I", "N", "!"};
    private static String[] question = {"When did the World War II begin?"};
    private static String[][] arraysMemDate =
            {
                    {"", "", "", "", "", "", "", "", "", ""},
                    {"", "", "", "", "", "", "", "", "", ""},
                    {"", "", "", "", "", "", "", "", "", ""},
                    {"", "", "", "", "", "", "", "", "", ""},
                    {"", "", "", "", "", "", "", "", "", ""},
                    {"", "", "", "", "", "", "", "", "", ""},
                    {"", "", "", "", "", "", "", "", "", ""},
                    {"", "", "", "", "", "", "", "", "", ""},
                    {"", "", "", "", "", "", "", "", "", ""},
                    {"", "", "", "", "", "", "", "", "", ""},

//                    {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"},
//                    {"11", "12", "13", "14", "15", "16", "17", "18", "19", "20"},
//                    {"21", "22", "23", "24", "25", "26", "27", "28", "29", "30"},
//                    {"31", "32", "33", "34", "35", "36", "37", "38", "39", "40"},
//                    {"41", "42", "43", "44", "45", "46", "47", "48", "49", "50"},
//                    {"51", "52", "53", "54", "55", "56", "57", "58", "59", "60"},
//                    {"61", "62", "63", "64", "65", "66", "67", "68", "69", "70"},
//                    {"71", "72", "73", "74", "75", "76", "77", "78", "79", "80"},
//                    {"81", "82", "83", "84", "85", "86", "87", "88", "89", "90"},
//                    {"91", "92", "93", "94", "95", "96", "97", "98", "99", "100"},
            };

    public static void main(String[] args) {
        System.out.println(resultRunRepetitions.getByRepetition(statArrays));
        System.out.print("Enter your name: ");
        name = in.nextLine();

        do {
            System.out.println("\n" + resultRunRepetitions.getByRepetition(question));
            System.out.print("guess the number: ");

            resultMemorableDate.getByResult(arraysMemDate, RandomNumber.getByRand(0, 9), RandomNumber.getByRand(0, 9));

            CheckNumber.getBycheckNumber();


            while (userNumberMemory[userNumberMemory.length - 1] != randomNumberMemory) {

                CheckNumber.getBycheckNumber();


                for (int i = 0; i < userNumberMemory.length; i++) {
                    if (userNumberMemory[i] == 0) {
                        userNumberMemory[i] = in.nextInt();
                        break;
                    }
                }


                if (ElemNumberEndArray.getEndNumb(userNumberMemory) > 100) {
                    System.out.println("error outside the range of a digit is greater than the allowable value\nPlease, try again. ");
                }
                if (ElemNumberEndArray.getEndNumb(userNumberMemory) <= 0) {
                    System.out.println("error outside the range of a digit is less than the allowable value\nPlease, try again. ");
                }
                if (ElemNumberEndArray.getEndNumb(userNumberMemory) < randomNumberMemory && ElemNumberEndArray.getEndNumb(userNumberMemory) > 0) {
                    System.out.println("Your number is too small. Please, try again.");
                }
                if (ElemNumberEndArray.getEndNumb(userNumberMemory) > randomNumberMemory && ElemNumberEndArray.getEndNumb(userNumberMemory) <= 100) {
                    System.out.println("Your number is too big. Please, try again.");
                }
                if (ElemNumberEndArray.getEndNumb(userNumberMemory) == randomNumberMemory) {
                    break;
                }


            }

        } while (ElemNumberEndArray.getEndNumb(userNumberMemory) <= 0);
        System.out.println("Congratulations, " + name + " !");

        System.out.println("You entered numbers:: ");
        for (int i = 0; i < userNumberMemory.length; i++) {
            if (userNumberMemory[i] > 0) {
                System.out.print(userNumberMemory[i] + " ");
            }
        }
    }

    static class ArrayMemorableDates {
        String getByResult(String[][] array, int column, int row) {
            if (row <= 10) {
                randomNumberMemory = Integer.parseInt(column + "" + row);
                return array[column][row];
            }
            return "Fatal error not range";
        }
    }

    static class ArrayRunRepetitions {
        String getByRepetition(String[] array) {
            for (byte i = 0; i < array.length; i++) {
                System.out.print(array[i]);
            }
            return "\n";
        }
    }

    static class RandomNumber {
        static int getByRand(int min, int max) {
            max -= min;
            return (int) (Math.random() * ++max) + min;
        }
    }

    static class CheckNumber {
        static void getBycheckNumber() {
            while (!in.hasNextInt()) {
                System.out.print("That not a number! Try again: ");
                in.next();
            }
        }
    }

    static class ElemNumberEndArray {
        private static int value;
        static int getEndNumb(int array[]) {
            for (int i = 0; i < array.length; i++) {
                if (array[i] > 0) {
                    value = array[i];
                }
            }
            return value;
        }
    }

}

