package com.company;

import java.util.Arrays;
import java.util.Scanner;

public class Homework_2 {
    private static Scanner in = new Scanner(System.in);
    private static String shot;
    private static String nullMarker = "0";
    private static String separator = "|";
    private static String[] memoryTarget = new String[3];
    private static String[][] arraysCub =
            {
                    {" - ", " - ", " - ", " - ", " - "},
                    {" - ", " - ", " - ", " - ", " - "},
                    {" - ", " - ", " - ", " - ", " - "},
                    {" - ", " - ", " - ", " - ", " - "},
                    {" - ", " - ", " - ", " - ", " - "},
            };
    private static String[][] markerArray =
            {
                    {separator + " " + "1", separator + " " + "2", separator + " " + "3", separator + " " + "4", separator + " " + "5", separator},
                    {"1" + " " + separator, "2" + " " + separator, "3" + " " + separator, "4" + " " + separator, "5" + " " + separator, separator},
            };

    public static void main(String[] args) {
        System.out.println("All set. Get ready to rumble!");
        getRender();
        getByTarget();
    }

    private static String[][] clone = arraysCub.clone();

    static void getRender() {

        System.out.print(nullMarker + " ");
        System.out.println(Arrays.toString(markerArray[0]).replaceAll("[^- x1-2-3-4-50|]", ""));
        for (int i = 0; i < clone.length; i++) {
            System.out.print(markerArray[1][i]);
            System.out.println(Arrays.toString(clone[i]).replaceAll("[^- x 1-2-3-4-5|*]", ""));
        }
    }

    static void getByTarget() {

        int valueX;
        int valueY;


        while (true) {
            valueX = getByRand(0, 3);
            valueY = getByRand(0, 3);
            if (valueX == valueY) {

                if (getTrack(valueX) != 2
                        && getTrack(valueX) != 0
                        && getTrack(valueX) != 4) {

                    if (getByRand(0, 1) == 1) {
                        getPosRender(valueX, 2, true);
                    } else {
                        getPosRender(valueX, 2, false);
                    }

                    while (true) {


                        shot = in.nextLine();
                        int st = Integer.parseInt(String.valueOf(shot.charAt(0))) - 1;
                        int en = Integer.parseInt(String.valueOf(shot.charAt(shot.length() - 1))) - 1;

                        if (st <= 4 && en <= 4 && st != -1 && en != -1) {
                            getShotAttack(Integer.toString(st) + en);
                            getEnd();
                        } else {
                            System.out.print("Incorrect input. Please, try again.: ");
                        }
                    }
                }
            }
        }
    }


    static int getByRand(int min, int max) {
        max -= min;
        return (int) (Math.random() * ++max) + min;
    }

    static int getTrack(int valdata) {
        for (int i = 0; i <= 3; i++) {
            if (valdata == i) {
                return i;
            }
        }
        return -1;
    }


    static void getPosRender(int value, int trackF, boolean bolTrack) {

        if (bolTrack) {
            if (value == 3) {
                if (getByRand(0, 1) == 0) {
                    memoryTarget[0] = "02";
                    memoryTarget[1] = "12";
                    memoryTarget[2] = "22";
                } else {
                    if (getByRand(0, 1) == 0) {
                        memoryTarget[0] = "01";
                        memoryTarget[1] = "11";
                        memoryTarget[2] = "21";
                    } else {
                        memoryTarget[0] = "00";
                        memoryTarget[1] = "10";
                        memoryTarget[2] = "20";
                    }
                }
            } else {
                if (getByRand(0, 1) == 0) {
                    memoryTarget[0] = "32";
                    memoryTarget[1] = "42";
                    memoryTarget[2] = "22";
                } else {
                    if (getByRand(0, 1) == 0) {
                        memoryTarget[0] = "33";
                        memoryTarget[1] = "43";
                        memoryTarget[2] = "23";
                    } else {
                        memoryTarget[0] = "34";
                        memoryTarget[1] = "44";
                        memoryTarget[2] = "24";
                    }
                }
            }
        } else {
            if (value == 3) {
                if (getByRand(0, 1) == 0) {
                    memoryTarget[0] = "20";
                    memoryTarget[1] = "21";
                    memoryTarget[2] = "22";
                } else {
                    if (getByRand(0, 1) == 0) {
                        memoryTarget[0] = "10";
                        memoryTarget[1] = "11";
                        memoryTarget[2] = "12";
                    } else {
                        memoryTarget[0] = "00";
                        memoryTarget[1] = "01";
                        memoryTarget[2] = "02";
                    }
                }
            } else {
                if (getByRand(0, 1) == 0) {
                    memoryTarget[0] = "23";
                    memoryTarget[1] = "24";
                    memoryTarget[2] = "22";
                } else {
                    if (getByRand(0, 1) == 0) {
                        memoryTarget[0] = "33";
                        memoryTarget[1] = "34";
                        memoryTarget[2] = "32";
                    } else {
                        memoryTarget[0] = "43";
                        memoryTarget[1] = "44";
                        memoryTarget[2] = "42";
                    }
                }
            }
        }
    }


    static void getShotAttack(String shot) {


        for (int i = 0; i < memoryTarget.length; i++) {
            if (memoryTarget[i].equals(shot)) {
                arraysCub[Integer.parseInt(String.valueOf(memoryTarget[i].charAt(memoryTarget[i].length() - 1)))]
                        [Integer.parseInt(String.valueOf(memoryTarget[i].charAt(0)))] = " x ";
                break;
            } else {
                arraysCub[Integer.parseInt(String.valueOf(shot.charAt(shot.length() - 1)))]
                        [Integer.parseInt(String.valueOf(shot.charAt(0)))] = " * ";
            }
        }
        getRender();
    }

    static void getEnd() {
        if (Arrays.deepToString(arraysCub).replaceAll("[^x]", "").equals("xxx")) {
            System.out.println("You have won!");
            System.exit(0);
        }
    }


}
